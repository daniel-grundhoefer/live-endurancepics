$(function () {
	
    var offset = 220;
    var duration = 500;
    $(window).scroll(function() {
        if ($(this).scrollTop() > offset) {
            $('.back-to-top').fadeIn(duration);
        } else {
            $('.back-to-top').fadeOut(duration);
        }
    });
    
    $('.back-to-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, duration);
        return false;
    })
	
    $("#btnSearch").click(function () {
    	var scrollTo = "#" + $("#txtSearch").val();
    	console.log(scrollTo);
        event.preventDefault();
        $('html, body').animate({
        	scrollTop: $(scrollTo).offset().top
        	}, duration);
        return false;
	});
    
	var bStorage = supportsStorage();
	
	if (bStorage) {
		sessionStorage.lastUpdate = Date.now();
	}
	
    $("#btnRefresh").click(function () {
    	var last = $("#hfLastFilter").val();
    	loadTiming(last, false, true);
    });
        
    loadTiming("all", bStorage, true);
});